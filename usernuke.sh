 #!/bin/bash

/sbin/mount -uw /

echo "Found the following users:"
ls -1 /Users/ | grep -v "Shared" | grep -v "Deleted Users" | grep -v "\."
declare -a userarray
userarray=( `ls /Users/ | grep -v "Shared" | grep -v "Deleted Users" | grep -v "\."` )
for (( i = 0 ; i < ${#userarray[@]} ; i++ ));
do 
	   echo -n "Delete user ${userarray[i]} (Y/N)?: "
           read -n 1 answer
           case "$answer" in
		y|Y)
		   rm -Rf "/Users/${userarray[i]}/"
		   echo ""
                   echo "${userarray[i]} deleted."
		;;
		n|N)
		    echo ""
                    echo "${userarray[i]} NOT deleted."
		;;
		*)
  		    echo ""
                    echo "$answer unknown. Please answer Y or N." 
		    let i=(i-1)
		;;
           esac 	
   echo ""
done
rm -Rf "/Users/Deleted Users/"
if [ -d /var/db/dslocal ]; then
     rm -Rf /var/db/dslocal
     mkdir -p /var/db/dslocal/nodes
     cp -Rp /System/Library/DirectoryServices/DefaultLocalDB/Default /var/db/dslocal/nodes/
     cp -Rp /System/Library/DirectoryServices/DefaultLocalDB/dsmappings /var/db/dslocal/
fi
rm -rf /private/var/db/netinfo
rm -rf /private/var/db/openldap
rm -rf /private/var/db/samba
rm -rf /private/var/db/dhcpclient
mv /var/db/.AppleSetupDone /var/db/.RunLanguageChooserToo
rm -rf /Library/Caches
rm -rf /Library/Logs
rm -rf /Library/Preferences.
rm /usernuke

echo "User Nuke successfully removed user databases."
echo ""
echo "Press any key to shutdown."
read -n 1 nothing
/sbin/fsck -fy
shutdown -h now
